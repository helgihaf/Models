﻿namespace Marsonsoft.Models.ModelSupport
{
    /// <summary>
    /// Represents an object model that has a unique identifier.
    /// </summary>
    /// <typeparam name="TId">The type of the unique identifier.</typeparam>
    public interface IIdentified<TId>
    {
        /// <summary>
        /// Gets or sets the unique identifier of the object.
        /// </summary>
        TId Id { get; set; }
    }
}
