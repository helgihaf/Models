﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Marsonsoft.Models.ModelSupport
{
    /// <summary>
    /// Represents a required, non-empty name with a maximium length.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class NameAttribute : StringLengthAttribute
    {
        /// <summary>
        /// The default <see cref="StringLengthAttribute.MaximumLength"/> value.
        /// </summary>
        public const int DefaultMaximumLength = 250;

        /// <summary>
        /// Initializes a new instance of the <see cref="NameAttribute"/> class.
        /// </summary>
        public NameAttribute()
            : this(DefaultMaximumLength)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NameAttribute"/> class with a specified maximum length.
        /// </summary>
        /// <param name="maxLength">The maximum length of a string.</param>
        public NameAttribute(int maxLength)
            : base(maxLength)
        {
            MinimumLength = 1;
        }
    }
}
