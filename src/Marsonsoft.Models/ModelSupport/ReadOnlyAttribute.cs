﻿using System;

namespace Marsonsoft.Models.ModelSupport
{
    /// <summary>
    /// Represents a read-only member, that is, a member variable that does not accept values from
    /// outside of the application.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class ReadOnlyAttribute : Attribute
    {
    }
}
