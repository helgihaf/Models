﻿using System.ComponentModel.DataAnnotations;

namespace Marsonsoft.Models.ModelSupport
{
    /// <summary>
    /// Represents free-text, optional notes with a maximum length.
    /// </summary>
    public class NotesAttribute : StringLengthAttribute
    {
        /// <summary>
        /// The default <see cref="StringLengthAttribute.MaximumLength"/> value.
        /// </summary>
        public const int DefaultMaximumLength = 2000;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotesAttribute"/> class.
        /// </summary>
        public NotesAttribute()
            : this(DefaultMaximumLength)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotesAttribute"/> class with a specified maximum length.
        /// </summary>
        /// <param name="maxLength">The maximum length of a string.</param>
        public NotesAttribute(int maxLength)
            : base(maxLength)
        {
        }
    }
}
