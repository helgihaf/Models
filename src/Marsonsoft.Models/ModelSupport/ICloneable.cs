﻿namespace Marsonsoft.Models.ModelSupport
{
    /// <summary>
    /// Represents an object model that can create a deep clone of itself.
    /// </summary>
    /// <remarks>
    /// Deep clone means all non-immutable member properties are cloned, otherwised
    /// a reference to the original (immutable) member is used.
    /// </remarks>
    /// <typeparam name="TModel">The model.</typeparam>
    public interface ICloneable<TModel> where TModel : class
    {
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        TModel Clone();
    }
}
