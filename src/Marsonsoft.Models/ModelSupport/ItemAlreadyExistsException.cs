﻿using System;

namespace Marsonsoft.Models.ModelSupport
{
    /// <summary>
    /// The exception that is thrown when an item is being added to a repository and it already exists there.
    /// </summary>
    public class ItemAlreadyExistsException : Exception
    {
        private string propertyName;

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemAlreadyExistsException"/> class.
        /// </summary>
        public ItemAlreadyExistsException() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemAlreadyExistsException"/> class with a specified error message.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public ItemAlreadyExistsException(string message) : base(message) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemAlreadyExistsException"/> class with a specified error message
        /// and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner"/> parameter
        /// is not a null reference, the current exception is raised in a <c>catch</c> block that handles the inner exception.</param>
        public ItemAlreadyExistsException(string message, Exception inner) : base(message, inner) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemAlreadyExistsException"/> class with a specified error message
        /// and the name of the property that causes this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="propertyName">The name of the property that caused the current exception.</param>
        public ItemAlreadyExistsException(string message, string propertyName)
            : base(message)
        {
            this.propertyName = propertyName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemAlreadyExistsException"/> class with a specified error message,
        /// he name of the property that causes this exception and a reference to the inner exception that is the cause of
        /// this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="propertyName">The name of the property that caused the current exception.</param>
        /// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner"/> parameter
        /// is not a null reference, the current exception is raised in a <c>catch</c> block that handles the inner exception.</param>
        public ItemAlreadyExistsException(string message, string propertyName, Exception inner)
            : base(message, inner)
        {
            this.propertyName = propertyName;
        }

        /// <summary>
        /// Gets the name of the property that causes this exception.
        /// </summary>
        public string PropertyName
        {
            get { return propertyName; }
        }

        /// <summary>
        /// Gets the error message and the property name, or only the error message if no property name is set.
        /// </summary>
        public override string Message
        {
            get
            {
                string s = base.Message;
                if (!string.IsNullOrEmpty(propertyName))
                {
                    s = s + Environment.NewLine + "PropertyName: " + propertyName;
                }
                return s;
            }
        }
    }
}
