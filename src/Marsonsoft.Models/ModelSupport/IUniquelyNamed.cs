﻿namespace Marsonsoft.Models.ModelSupport
{
    /// <summary>
    /// Represents a model object that has a unique name.
    /// </summary>
    public interface IUniquelyNamed
    {
        /// <summary>
        /// Gets or sets the unique name of the object.
        /// </summary>
        string Name { get; set; }
    }
}
