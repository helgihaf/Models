﻿using System.Collections.Generic;

namespace Marsonsoft.Models.RepositorySupport
{
    /// <summary>
    /// Represents a collection of paged model items.
    /// </summary>
    /// <typeparam name="TModel">The type of the model in the collection.</typeparam>
    public class PagedItems<TModel>
    {
        /// <summary>
        /// Gets or sets the total number of items in the collection.
        /// </summary>
        public int TotalItems { get; set; }

        /// <summary>
        /// Gets or sets an enumeration of the model items in the collection.
        /// </summary>
        public IEnumerable<TModel> Items { get; set; }
    }
}
