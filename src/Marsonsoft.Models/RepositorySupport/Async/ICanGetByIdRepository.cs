﻿using System.Threading.Tasks;

namespace Marsonsoft.Models.RepositorySupport.Async
{
    /// <summary>
    /// Represents an async repository that can get model items by their unique ID.
    /// </summary>
    /// <typeparam name="TModel">The type of the model in the repository.</typeparam>
    /// <typeparam name="TId">The type of the unique identifier of TModel.</typeparam>
    public interface ICanGetByIdRepository<TModel, TId>
    {
        /// <summary>
        /// Gets the item with the specified ID.
        /// </summary>
        /// <param name="id">The ID of the item to get.</param>
        /// <returns>The item with the specified ID or <c>default(TModel)</c> if the item was not found.</returns>
        Task<TModel> GetAsync(TId id);
    }
}
