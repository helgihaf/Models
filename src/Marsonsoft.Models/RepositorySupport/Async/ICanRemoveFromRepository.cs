﻿using System.Threading.Tasks;

namespace Marsonsoft.Models.RepositorySupport.Async
{
    /// <summary>
    /// Represents an async repository that can remove model items.
    /// </summary>
    /// <typeparam name="TId">The type of the unique identifier of TModel.</typeparam>
    public interface ICanRemoveFromRepository<TId>
    {
        /// <summary>
        /// Removes the item with a specified ID from the repository.
        /// </summary>
        /// <param name="id">The ID of the item to remove.</param>
        /// <returns>An instance of the task representing the async operation.</returns>
        Task RemoveAsync(TId id);
    }
}
