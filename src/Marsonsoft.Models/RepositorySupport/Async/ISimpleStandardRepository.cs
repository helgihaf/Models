﻿namespace Marsonsoft.Models.RepositorySupport.Async
{
    /// <summary>
    /// Represents a simple "standard" async repository with the most basic repository capabilities.
    /// </summary>
    /// <typeparam name="TModel">The type of the model in the repository.</typeparam>
    /// <typeparam name="TId">The type of the unique identifier of TModel.</typeparam>
    public interface ISimpleStandardRepository<TModel, TId>
        : ICanGetByIdRepository<TModel, TId>, ICanAddToRepository<TModel, TId>, ICanEditRepository<TModel, TId>, 
            ICanRemoveFromRepository<TId>, ICanGetManyFromRepository<TModel>
        where TModel : class, ModelSupport.IIdentified<TId>
    {
    }
}
