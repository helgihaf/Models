﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marsonsoft.Models.RepositorySupport.Async
{
    /// <summary>
    /// Represents an async repository that can sort model items.
    /// </summary>
    /// <typeparam name="TModel">The type of the model in the repository.</typeparam>
    public interface ICanSortRepository<TModel>
    {
        /// <summary>
        /// Gets multiple items from the repository sorted as specified in the sorting instructions.
        /// </summary>
        /// <param name="sortingInstructions">An enumerable of sorting instructions.</param>
        /// <returns>Items sorted according to the sorting instructions.</returns>
        Task<IEnumerable<TModel>> GetSortedAsync(IEnumerable<SortingInstruction> sortingInstructions);
    }
}
