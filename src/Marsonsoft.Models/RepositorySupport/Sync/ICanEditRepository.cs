﻿using System;
using System.Threading.Tasks;

namespace Marsonsoft.Models.RepositorySupport.Sync
{
    /// <summary>
    /// Represents a repository that can edit model items.
    /// </summary>
    /// <typeparam name="TModel">The type of the model in the repository.</typeparam>
    /// <typeparam name="TId">The type of the unique identifier of TModel.</typeparam>
    public interface ICanEditRepository<TModel, TId>
        where TModel : class, ModelSupport.IIdentified<TId>
    {
        /// <summary>
        /// Edits the model in the repository identified by <paramref name="id"/> by effectively replacing it
        /// with <paramref name="model"/>.
        /// </summary>
        /// <param name="id">The Id of the model to edit.</param>
        /// <param name="model">The new version of the model.</param>
        /// <returns>The model as it exists after editing it in the repository.</returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="model"/></exception>
        /// <exception cref="ModelSupport.ItemAlreadyExistsException">A model with the same value as one of the model's unique properties already exists in the repository.</exception>
        /// <remarks>
        /// The <see cref="ModelSupport.IIdentified{TId}.Id"/> property of <paramref name="model"/> is ignored.
        /// </remarks>
        TModel Edit(TId id, TModel model);

        /// <summary>
        /// Edits the model in the repository identified by <paramref name="id"/> by applying the changes
        /// in <paramref name="modelPatchDocument"/> to it.
        /// </summary>
        /// <param name="id">The Id of the model to edit.</param>
        /// <param name="modelPatchDocument">A JSON Patch Document describing the changes to make on the existing model.</param>
        /// <param name="validateModelAction">Action that validates the patched model or throws an exception if not valid.</param>
        /// <returns>The model as it exists after editing it in the repository.</returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="modelPatchDocument"/>, <paramref name="validateModelAction"/></exception>
        /// <exception cref="ModelSupport.ItemAlreadyExistsException">A model with the same value as one of the model's unique properties already exists in the repository.</exception>
        /// <remarks>
        /// The only operations supported in the JSON Patch Document is "replace". See <see href="http://jsonpatch.com/"/>.
        /// Changes to the <see cref="ModelSupport.IIdentified{TId}.Id"/> property are not allowed.
        /// </remarks>
        TModel PartialEdit(TId id, Microsoft.AspNetCore.JsonPatch.JsonPatchDocument<TModel> modelPatchDocument, Action<TModel> validateModelAction);
    }
}
