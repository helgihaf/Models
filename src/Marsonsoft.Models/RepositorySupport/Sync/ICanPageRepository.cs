﻿using System.Threading.Tasks;

namespace Marsonsoft.Models.RepositorySupport.Sync
{
    /// <summary>
    /// Represends a repository that can get multiple model items one page at a time.
    /// </summary>
    /// <typeparam name="TModel">The type of the model in the repository.</typeparam>
	public interface ICanPageRepository<TModel>
	{
        /// <summary>
        /// Gets a page of model items based on specified paging instructions.
        /// </summary>
        /// <param name="pagingInstructions">Instructions on which page to get.</param>
        /// <returns>A <see cref="PagedItems{T}"/> object containing a page of model items.</returns>
		PagedItems<TModel> GetPaged(PagingInstructions pagingInstructions);
	}

}
