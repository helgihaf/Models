﻿using System.Threading.Tasks;

namespace Marsonsoft.Models.RepositorySupport.Sync
{
    /// <summary>
    /// Represents a repository that can remove model items.
    /// </summary>
    /// <typeparam name="TId">The type of the unique identifier of TModel.</typeparam>
    public interface ICanRemoveFromRepository<TId>
    {
        /// <summary>
        /// Removes the item with a specified ID from the repository.
        /// </summary>
        /// <param name="id">The ID of the item to remove.</param>
        void Remove(TId id);
    }
}
