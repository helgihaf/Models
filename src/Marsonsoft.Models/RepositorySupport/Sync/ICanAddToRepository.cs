﻿using System.Threading.Tasks;

namespace Marsonsoft.Models.RepositorySupport.Sync
{
    /// <summary>
    /// Represents a repository that can add model items.
    /// </summary>
    /// <typeparam name="TModel">The type of the model in the repository.</typeparam>
    /// <typeparam name="TId">The type of the unique identifier of TModel.</typeparam>
    public interface ICanAddToRepository<TModel, TId>
        where TModel : class, Marsonsoft.Models.ModelSupport.IIdentified<TId>
    {
        /// <summary>
        /// Adds the specified model to the repository, assigning it a new Id.
        /// </summary>
        /// <param name="model">The model to add.</param>
        /// <returns>The model as it exists after adding it to the repository.</returns>
        /// <exception cref="System.ArgumentNullException">model</exception>
        /// <exception cref="ModelSupport.ItemAlreadyExistsException">A model with the same value as one of the model's unique properties already exists in the repository.</exception>
        TModel Add(TModel model);
    }
}
