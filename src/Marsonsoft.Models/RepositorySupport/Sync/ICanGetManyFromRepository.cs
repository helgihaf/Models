﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marsonsoft.Models.RepositorySupport.Sync
{
    /// <summary>
    /// Represents a repository that can get multiple model items.
    /// </summary>
    /// <typeparam name="TModel">The type of the model in the repository.</typeparam>
    public interface ICanGetManyFromRepository<TModel>
	{
        /// <summary>
        /// Gets all model items in the repository.
        /// </summary>
        /// <returns>All the model items in the repository.</returns>
        IEnumerable<TModel> GetMany();
	}
}
