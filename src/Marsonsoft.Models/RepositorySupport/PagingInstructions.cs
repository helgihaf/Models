﻿using System.Collections.Generic;

namespace Marsonsoft.Models.RepositorySupport
{
    /// <summary>
    /// Instructions on how to page a set of arbitrary items.
    /// </summary>
    public class PagingInstructions
    {
        /// <summary>
        /// Gets or sets the number of the page.
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// Gets or sets the page size, that is, the number of items on a page.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Gets or sets an enumeration of sorting instructions on which the paging is based.
        /// </summary>
        public IEnumerable<SortingInstruction> SortingInstructions { get; set; }
    }
}
