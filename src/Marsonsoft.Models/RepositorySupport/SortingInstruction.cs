﻿namespace Marsonsoft.Models.RepositorySupport
{
    /// <summary>
    /// Instruction on how to sort a property of an arbitrary item.
    /// </summary>
    public class SortingInstruction
    {
        /// <summary>
        /// Gets or sets the name of the property.
        /// </summary>
        public string Property { get; set; }

        /// <summary>
        /// Gets or sets the sorting direction.
        /// </summary>
        public SortingDirection Direction { get; set; }
    }

    /// <summary>
    /// Represent a sorting direction.
    /// </summary>
    public enum SortingDirection
    {
        /// <summary>
        /// Sort in ascending order.
        /// </summary>
        Ascending,

        /// <summary>
        /// Sort in descending order.
        /// </summary>
        Descending
    }
}
